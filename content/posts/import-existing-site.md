---
title: "Import Existing Site"
date: 2023-07-31T21:37:28+02:00
draft: false
---
# Summary
This post serves as a helpful reminder for myself on how to restore the markdown files.

## Steps to Perform
To restore the markdown files, follow these steps:

1. Clone the repository along with its submodules using the following command:
   ```
   git clone --recurse-submodules https://my-git-url
   ```

2. Change your current directory to the cloned repository using the following command:
   ```
   cd repository/
   ```

3. Generate the static files and markdown content using Hugo by running the command:
   ```
   hugo
   ```

By following these steps, you should be able to successfully restore the markdown files and generate the static content for your project. Happy restoring!